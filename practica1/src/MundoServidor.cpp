// MundoServidor.cpp: implementation of the CMundo class.
//Alexandre Cottini 51705
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <error.h>
/*#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>*/
#include <iostream>
#include <pthread.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void* hilo_comandos(void* d)
{
	CMundoServidor* p=(CMundoServidor*) d;
	p->RecibeComandosJugador();
}

void CMundoServidor::RecibeComandosJugador()
{
	while(1)
	{
		usleep(10);
		char cadena[100];
		//read(tuberiateclas,cadena,sizeof(cadena));
		socket_common.Receive(cadena,sizeof(cadena));
		unsigned char key;
		sscanf(cadena,"%c",&key);
		if(key=='s')jugador1.velocidad.y=-4;
		if(key=='w')jugador1.velocidad.y=4;
		if(key=='l')jugador2.velocidad.y=-4;
		if(key=='o')jugador2.velocidad.y=4;
	}
}

CMundoServidor::CMundoServidor()
{
	Init();
}

CMundoServidor::~CMundoServidor()
{
	char cierre[200];
	sprintf(cierre, "Fin de la partida");
	write (tuberia,cierre,strlen(cierre)+1);
	close (tuberia);
	
	/*pMemComp->accion1=2;	
	munmap(pMemComp,sizeof(MemComp));
	close(tuberiacs);
	close(tuberiateclas);*/
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
		char cad [210];
		sprintf(cad,"Jugador2 marca 1 punto, lleva %d", puntos2);
		write(tuberia,cad,strlen(cad)+1);
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

		char cad [210];
		sprintf(cad,"Jugador1 marca 1 punto, lleva %d", puntos1);
		write(tuberia,cad,strlen(cad)+1);
	}
	if(jugador1.Rebota(esfera)){
		jugador1.y1-=0.2;
		jugador2.y1+=0.2;
	}
	if(jugador2.Rebota(esfera)){
		jugador1.y1+=0.2;
		jugador2.y1-=0.2;
	}
	
	/*
	pMemComp->esfera=esfera;
	pMemComp->raqueta1=jugador1;
	pMemComp->raqueta2=jugador2;
	

	//Implementación sobre el bot
	switch(pMemComp->accion)
	{
		case 1: jugador1.velocidad.y=4; 			
			break;
		case 0: break;
		case -1: jugador1.velocidad.y=-4;
			break;
	}*/


	//El juego por alcanzar el límite de puntos

	int EstadoActual=0;
	int EstadoAnterior=0;
	char cadena[200];

	if(puntos1==3)
	{
		EstadoActual=1;
		/*if(EstadoActual!=EstadoAnterior)
		{
			sprintf(cadena,"Jugador 1 ha marcado 3 puntos y gana la partida");
			write(tuberia,cadena,strlen(cadena)+1);
		}*/

		EstadoAnterior=EstadoActual;
		exit(0);
	}

	if(puntos2==3)
	{
		EstadoActual=1;
		/*if(EstadoActual!=EstadoAnterior)
		{
			sprintf(cadena,"Jugador 2 ha marcado 3 puntos y gana la partida");
			write(tuberia,cadena,strlen(cadena)+1);
		}*/

		EstadoAnterior=EstadoActual;
		exit(0);
	}
	char cadcs[200];
	sprintf(cadcs,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y,jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2,puntos1,puntos2);
	//std::cout << "CS: " << cadcs << "\n" ;
	//write(tuberiacs,cadcs,sizeof(cadcs));
	socket_common.Send(cadcs,sizeof(cadcs));
}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
}

void CMundoServidor::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	MemComp.raqueta1=jugador1;
	

	const char*myFifo="/tmp/fifo";
	tuberia=open(myFifo,O_WRONLY);
	/*
	
	const char*myBot="/tmp/bot";
	int file=open(myBot,O_RDWR|O_CREAT|O_TRUNC, 0666);
	write(file,&MemComp,sizeof(MemComp));
	pMemComp=(DatosMemCompartida*)mmap(NULL,sizeof(MemComp),PROT_WRITE|PROT_READ,MAP_SHARED,file,0); 
	close(file);
	pMemComp->accion=0; */

	//tuberiacs=open("/tmp/csfifo",O_WRONLY);
	pthread_create(&thid1,NULL,hilo_comandos,this);
	//tuberiateclas=open("/tmp/teclasfifo",O_RDONLY);
	socket_nexus.InitServer((char*)"127.0.0.01",4800);
	socket_common=socket_nexus.Accept();

	char nombre_cliente[100];
	socket_common.Receive(nombre_cliente,sizeof(nombre_cliente));
	printf("Conectado el cliente, %s\n", nombre_cliente);	
	

}
