//Alexandre Cottini 51705

#pragma once
#include "Raqueta.h"
#include "Esfera.h"

class DatosMemCompartida
	{
	public:
		Esfera esfera;
		Raqueta raqueta1;
		Raqueta raqueta2;
	 	int accion; //1 arriba, 0 nada, -1 abajo
	};
